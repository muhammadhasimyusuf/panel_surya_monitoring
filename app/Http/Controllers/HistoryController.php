<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\HistoryDataTable;
use App\Services\RekapService;
use Carbon\Carbon;

class HistoryController extends Controller
{

    protected $rekapService;

    public function __construct(RekapService $rekapService)
    {
        $this->rekapService = $rekapService;
    }
    public function index(HistoryDataTable $dataTable){
      
        return $dataTable->render('History');
    }


    public function getRekapData(){
        $thresholds = [
            ["range_time" => "06:00-08:00", "threshold_voltage" => 13.5, "threshold_lux" => 25000],
            ["range_time" => "08:00-10:00", "threshold_voltage" => 13.5, "threshold_lux" => 25000],
            ["range_time" => "10:00-12:00", "threshold_voltage" => 14.0, "threshold_lux" => 30000],
            ["range_time" => "12:00-14:00", "threshold_voltage" => 14.2, "threshold_lux" => 32000],
            ["range_time" => "14:00-16:00", "threshold_voltage" => 14.0, "threshold_lux" => 30000]
        ];

        $rekapData = $this->rekapService->rekapPer30MenitPerRange($thresholds);
        
        return datatables()->of($rekapData)->toJson();
    }

    public function recapShow() {
        return view('Recap');
    }
}
