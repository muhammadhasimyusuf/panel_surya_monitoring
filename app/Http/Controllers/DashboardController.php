<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\realtimesensor;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index(){
        



        return view('Dashboard');
    }

    public function datasensor(){
        
        $dataSensor =  realtimesensor::orderBy('created_at', 'DESC')->first();
        return response($dataSensor, 200);
    }

    public function sensorcondition(){
        
        $latestTime = realtimesensor::latest('created_at')->value('created_at');
    
            $from = Carbon::parse($latestTime)->subMinutes(5);
            $to = $latestTime;
            $dataSensor = realtimesensor::whereBetween('created_at', [$from, $to])->get();
    
        return response($dataSensor, 200);

        

        
    }
}
