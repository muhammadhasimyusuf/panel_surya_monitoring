<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\realtimesensor;

class DataInputController extends Controller
{

    public function input(Request $request){
     $created =  realtimesensor::create($request->all());
    if($created){
        // return response('Created', 200);
        return response()->json(['result' => $created, 'state' => 'created'], 200, [], JSON_PRETTY_PRINT);
    }
    return  response('Not Created', 400);
    }
}
