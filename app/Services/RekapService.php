<?php

namespace App\Services;

use App\Models\ReadingCondition;
use App\Models\realtimesensor;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RekapService
{
    public static function rekapPer30MenitPerRange($thresholds)
    {
        $rekapData = [];
        
        // Group the data by date using a single query
        $datas = realtimesensor::select(DB::raw('DATE(DATE_ADD(created_at, INTERVAL 8 HOUR)) as date'))
            ->groupBy('date')
            ->orderBy('date', 'asc')
            ->get();

        foreach ($datas as $data) {
            $date = $data->date;
            $segments = [];

            // Get the start and end date for the current day
            $currentDate = Carbon::parse($date)->startOfDay();
            $endDate = Carbon::parse($date)->endOfDay();

            while ($currentDate <= $endDate) {
                $currentSegmentEnd = $currentDate->copy()->addMinutes(30);

                // Fetch data for the current 30-minute segment
                $segmentData = realtimesensor::whereBetween('created_at', [$currentDate, $currentSegmentEnd])
                    ->get();

                if (!$segmentData->isEmpty()) {
                    // Calculate average values
                    $avgTeganganPV = $segmentData->avg('tegangan_pv');
                    $avgTeganganBat = $segmentData->avg('tegangan_bat');
                    $avgArusPV = $segmentData->avg('arus_pv');
                    $avgIntensitasCahaya = $segmentData->avg('intensitas_cahaya');

                    $segments[] = [
                        'waktu_mulai' => $currentDate->copy()->format('H:i'),
                        'waktu_selesai' => $currentSegmentEnd->copy()->format('H:i'),
                        'rekap' => [
                            'tegangan_pv' => $avgTeganganPV,
                            'tegangan_bat' => $avgTeganganBat,
                            'arus_pv' => $avgArusPV,
                            'intensitas_cahaya' => $avgIntensitasCahaya,
                            "waktu" =>  $currentSegmentEnd->copy()->format('H:i'),
                            'kondisi' => ReadingCondition::evaluateCondition($avgTeganganPV, $avgIntensitasCahaya,  $currentSegmentEnd->copy()->format('H:i')),
                        ],
                        'data' => $segmentData
                    ];
                }

                $currentDate = $currentSegmentEnd;
            }

            // Add data for the current day to rekapData
            $rekapData[] = [
                'tanggal' => $date,
                'segments' => $segments,
            ];
        }

        return $rekapData;
    }
}
