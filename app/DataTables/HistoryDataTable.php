<?php

namespace App\DataTables;

use App\Models\realtimesensor;
use App\Models\ReadingCondition;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HistoryDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */

     protected $startDate;
    protected $endDate;

    public function __construct(Request $request)
    {
        if($request){
        function subHour($now, $sub){
            $date = $now;
            $carbon_date = Carbon::parse($date);
            return $carbon_date->subHours($sub)->format('Y-m-d\TH:i');
        }
        $this->startDate = $request->start_date ? subHour($request->start_date, 8) : null;
        $this->endDate = $request->end_date ? subHour($request->end_date, 8) : null;
        }
        parent::__construct();
    }
   
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        
        return (new EloquentDataTable($query))
            ->addColumn('action', 'realtimesensor.action')
            ->setRowId('id')
            ->editColumn('created_at', function ($res) {
                $date = $res->created_at;
                $carbon_date = Carbon::parse($date);
                $carbon_date->addHours(8);
                $time = $carbon_date->format('d-m-Y H:i'); 
                return $time;
            })
            ->editColumn('Kondisi', function ($res) {
                $date = $res->created_at;
                $carbon_date = Carbon::parse($date);
                $carbon_date->addHours(8);
                $time = $carbon_date->format('H:i');   
                
                
                $condition = ReadingCondition::evaluateCondition($res->tegangan_pv, $res->intensitas_cahaya, $time);
                return $condition;
            });

    }

    /**
     * Get the query source of dataTable.
     */
    public function query(realtimesensor $model): QueryBuilder
    {
        $query = $model->newQuery();
        
        if ($this->startDate && $this->endDate) {
             $query->whereBetween('created_at', [$this->startDate, $this->endDate]);
        }

        return $query;
    }

    /**
     * Optional method if you want to use the html builder.
     */
    
     public function html(): HtmlBuilder
     {
         $startDate = $this->request->input('start_date');
         $endDate = $this->request->input('end_date');
 
         return $this->builder()
            
             ->setTableId('realtimesensor-table')
             ->columns($this->getColumns())
             ->parameters([
                'dom'          => 'Bfrtip',
                'buttons'      => ['excel', 'csv'],
                 'lengthMenu' => [[10, 25, 50, -1], [10, 25, 50, 'All']],
                 'pageLength' => 25,
                 'order' => [[4, 'desc']],
                 'initComplete' => 'function(settings, json) {
                     $(\'input[name="daterange"]\').daterangepicker({
                         startDate: \'' . $startDate . '\',
                         endDate: \'' . $endDate . '\',
                         opens: \'left\',
                         locale: {
                             format: \'YYYY-MM-DD\',
                             cancelLabel: \'Clear\'
                         }
                     });
                 }',
             ])
             ->minifiedAjax()
             ->orderBy(5, 'DESC')
             ->selectStyleSingle()
             ->buttons([
                 Button::make('excel'),
                //  Button::make('csv'),
                //  Button::make('pdf'),
                //  Button::make('print'),
                //  Button::make('reset'),
                 Button::make('reload')
             ])
            //  ->dom('Blfrtip')
             ->parameters([
                 'initComplete' => 'function () {
                     this.api().columns([5]).every(function () {
                         var column = this;
                         var input = document.createElement("input");
                         $(input).appendTo($(column.footer()).empty())
                             .on("change", function () {
                                 column.search($(this).val(), false, false, true).draw();
                             });
                     });
                 }'
             ]);
     }
    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            // Column::computed('action')
            //       ->exportable(true)
            //       ->printable(false)
            //       ->width(60)
            //       ->addClass('text-center')->addClass('text-center'),
                  Column::make('tegangan_bat')->addClass('text-center'),
                  Column::make('tegangan_pv')->addClass('text-center'),
                  Column::make('arus_pv')->addClass('text-center'),
                  Column::make('intensitas_cahaya')->addClass('text-center'),
                  Column::make('created_at')->title('Waktu')->addClass('text-center'),
                  Column::make('Kondisi')->addClass('text-center')->orderable(false)->searchable(false),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'History_' . date('YmdHis');
    }
}
