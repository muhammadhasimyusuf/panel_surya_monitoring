<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;


class CreateRandomData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:create-random-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To Make Random Data Sensor';

    /**
     * Execute the console command.
     */private function generateRandomFloat($min, $max)
    {
        return round($min + mt_rand() / mt_getrandmax() * ($max - $min), 2);
    }

    private function generateRandomInt($min, $max)
    {
        return mt_rand($min, $max);
    }
    
    public function handle()
    {
        
        $response = Http::post("http://127.0.0.1:8000/api/input", [
            'tegangan_pv' => $this->generateRandomFloat(10, 15), 
            'tegangan_bat' => $this->generateRandomFloat(10, 15), 
            'arus_pv' => $this->generateRandomFloat(3, 7), 
            'intensitas_cahaya' => $this->generateRandomInt(2000, 8000), 
        ]);
        $contents = (string) $response->getBody();
        dump($contents);
        
    }
}
