<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReadingCondition extends Model
{
    protected $fillable = ['voltage', 'lux', 'time'];

    public static function evaluateCondition($voltage, $lux, $time)
    {
        $thresholds = [
            ["range_time" => "08:00-10:00", "threshold_voltage" => 12.10, "threshold_lux" => 25000],
            ["range_time" => "10:00-12:00", "threshold_voltage" => 12.10, "threshold_lux" => 50000],
            ["range_time" => "12:00-14:00", "threshold_voltage" => 12.10, "threshold_lux" => 50000],
            ["range_time" => "14:00-16:00", "threshold_voltage" => 12.10, "threshold_lux" => 50000],
            ["range_time" => "16:00-18:00", "threshold_voltage" => 12.10, "threshold_lux" => 35000]
        ];
        
        $threshold = null;
        $currentTime = DateTime::createFromFormat('H:i', $time);
    
        foreach ($thresholds as $item) {
            $range = explode('-', $item['range_time']);
            $start = DateTime::createFromFormat('H:i', trim($range[0]));
            $end = DateTime::createFromFormat('H:i', trim($range[1]));
    
            if ($currentTime >= $start && $currentTime <= $end) {
                $threshold = $item;
                break;
            }
        }


        
        if (!$threshold) {
            return "Malam Hari";
        }

        if ($voltage > $threshold['threshold_voltage'] && $lux < $threshold['threshold_lux']) {
            return "Normal";
        } elseif ($voltage < $threshold['threshold_voltage'] && $lux > $threshold['threshold_lux']) {
            return "Tidak Normal";
        } elseif ($voltage < $threshold['threshold_voltage'] && $lux < $threshold['threshold_lux']) {
            return "Normal";
        }

        return "Tidak Normal";

    }
}
