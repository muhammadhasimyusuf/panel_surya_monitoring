
<div class="w-full max-w-sm overflow-hidden rounded-lg bg-white shadow-md duration-300 hover:scale-105 hover:shadow-xl">
       
          <!-- <i class="fa-solid fa-solar-panel"></i> -->
          {!!$icon!!}
          <p class="my-4 text-center text-sm text-gray-500">
          {!!$value!!}
          </p>
          <div class="space-x-4 bg-gray-100 py-4 text-center">
               {{$title}}
            </div>
        </div>
