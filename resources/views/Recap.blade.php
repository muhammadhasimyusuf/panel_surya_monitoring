@extends('template.Main')

@section('head')
    <!-- Tambahkan CSS dan JavaScript yang diperlukan -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.css"> --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <style>
        #data-table td {
            text-align: center;
        }

        #data-table tbody tr:nth-child(even) {
            background-color: #f2f2f2;
            /* Warna untuk baris genap */
        }

        #data-table tbody tr:nth-child(odd) {
            background-color: #ffffff;
            /* Warna untuk baris ganjil */
        }
    </style>
    <script src="/vendor/datatables/buttons.server-side.js"></script>
@endsection

@section('content')
    <div class="container-fluid w-full text-center  max-w-[79%] p-5 ">
        <!-- Tabel Data -->
        <div class="row ">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Recap Data</div>
                    <div class="card-body">
                        <table id="data-table" class="display w-full">
                            <thead>
                                <tr role="row">
                                    <th>Tanggal</th>
                                    <th>Waktu Mulai</th>
                                    <th>Waktu Selesai</th>
                                    <th>Tegangan PV</th>
                                    <th>Tegangan Bat</th>
                                    <th>Arus PV</th>
                                    <th>Intensitas Cahaya</th>
                                    <th>Kondisi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('#data-table').DataTable({
                processing: true,
                serverSide: true,
                searching: false,

                ajax: '{{ route('history.rekap') }}',
                columns: [{
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'segments',
                        name: 'Waktu Mulai',
                        visible: false,
                        render: function(data, type, row) {
                            let waktuMulai = '';
                            if (data.length > 0) {
                                data.forEach(function(segment) {
                                    waktuMulai += '<div>' + segment.waktu_mulai + '</div>';
                                });
                            }
                            return waktuMulai;
                        }
                    },
                    {
                        data: 'segments',
                        name: 'Waktu Selesai',
                        render: function(data, type, row) {
                            let waktuSelesai = '';
                            if (data.length > 0) {
                                data.forEach(function(segment) {
                                    waktuSelesai += '<div>' + segment.waktu_selesai +
                                        '</div>';
                                });
                            }
                            return waktuSelesai;
                        }
                    },
                    {
                        data: 'segments',
                        name: 'Tegangan PV',
                        render: function(data, type, row) {
                            let teganganPV = '';
                            if (data.length > 0) {
                                data.forEach(function(segment) {
                                    teganganPV += '<div>' + segment.rekap.tegangan_pv
                                        .toFixed(2) + '</div>';
                                });
                            }
                            return teganganPV;
                        }
                    },
                    {
                        data: 'segments',
                        name: 'Tegangan Bat',
                        render: function(data, type, row) {
                            let teganganBat = '';
                            if (data.length > 0) {
                                data.forEach(function(segment) {
                                    teganganBat += '<div>' + segment.rekap.tegangan_bat
                                        .toFixed(2) + '</div>';
                                });
                            }
                            return teganganBat;
                        }
                    },
                    {
                        data: 'segments',
                        name: 'Arus PV',
                        render: function(data, type, row) {
                            let arusPV = '';
                            if (data.length > 0) {
                                data.forEach(function(segment) {
                                    arusPV += '<div>' + segment.rekap.arus_pv.toFixed(2) +
                                        '</div>';
                                });
                            }
                            return arusPV;
                        }
                    },
                    {
                        data: 'segments',
                        name: 'Intensitas Cahaya',
                        render: function(data, type, row) {
                            let intensitasCahaya = '';
                            if (data.length > 0) {
                                data.forEach(function(segment) {
                                    intensitasCahaya += '<div>' + segment.rekap
                                        .intensitas_cahaya.toFixed(2) + '</div>';
                                });
                            }
                            return intensitasCahaya;
                        }
                    },
                    {
                        data: 'segments',
                        name: 'Kondisi',
                        render: function(data, type, row) {
                            let kondisi = '';
                            if (data.length > 0) {
                                data.forEach(function(segment) {
                                    kondisi += '<div>' + segment.rekap.kondisi + '</div>';
                                });
                            }
                            return kondisi;
                        }
                    },
                ],


                lengthMenu: [
                    [-1],
                    ['All']
                ],
                dom: 'Bfrtip', // Menambahkan tombol
                buttons: [
                    'excelHtml5' // Tombol Export to Excel
                ],
                initComplete: function() {
                    let table = this.api();
                    table.rows().every(function() {
                        let data = this.data();
                        let row = this.node();

                        // if (data.segments.length === 0) {
                        //     $(row).find('td:eq(0)').hide(); // Hide Range Time
                        //     $(row).find('td:eq(1)').hide(); // Hide Tanggal
                        // }
                        $(row).find('td').css('text-align', 'center');
                    });
                }
            });
        });
    </script>
@endsection
