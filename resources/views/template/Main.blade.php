<!-- component -->
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  {{-- @vite(['resources/css/app.css', 'resources/js/app.js', 'resources/sass/app.scss']) --}}
  @vite(['resources/sass/app.scss', 'resources/js/app.js', 'resources/css/app.css'])
        
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    @yield('head')
<style>
      #header-main{
    background-image: url('/assets/img/image-removebg-preview.png');
  background-repeat: no-repeat;
  background-origin: content-box;
  background-size: 5% auto;
  background-position-x: right;
  background-position-y: bottom;
  } 
</style>
    </head>
    <body class="bg-gray-50">    
        @include('template.layout.Headers')
<div class="flex">
            {{-- @include('template.layout.Sidebar') --}}
            <main  class='w-full'>
                @yield('content')
            </main>
</div>

            <script src="https://kit.fontawesome.com/ced4463392.js" crossorigin="anonymous"></script>
            
            <!-- <script type="module">
        
            $(document).ready(function(){
                       console.log('aaaaa')
                       @yield('scriptReady')
                    });
            </script> -->
            @stack('script')
            @yield('script')
    </body>



<html>