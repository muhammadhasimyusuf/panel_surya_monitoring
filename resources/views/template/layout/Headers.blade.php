@php
    $arrSidebar = [
        [
            'link' => '/',
            'text' => 'Dashboard'
        ],
        [
            'link' => '/history',
            'text' => 'History'
        ],
        [
            'link' => '/recap',
            'text' => 'Rekap'
        ]
        ]
@endphp
<header id="header-main" class="bg-gray-800">
    <nav   class="container mx-auto px-6 py-3">
      <div class="flex items-center justify-between">
        <div class="text-white font-bold text-xl">
          <a href="#" class="text-decoration-none">
            
            <img class="w-[40px] h-[40px] shrink-0 inline-block rounded-[.95rem]" src="{{ URL::to('/') }}/assets/img/logo.png" alt="avatar image">
          </a>
        </div>
        <div>
            <h3 class="text-white">
                Sistem Monitoring Prediksi Dini PV
            </h3>
        </div>
        <div class="hidden md:block">
          <ul class="flex items-center space-x-8">
           @foreach($arrSidebar as $sidebar)
     
            <li><a href={{$sidebar['link']}} class="text-white cursor-pointer text-decoration-none">{{$sidebar['text']}}</a></li>
            @endforeach
          </ul>
        </div>
        <div class="md:hidden">
          <button class="outline-none mobile-menu-button">
            <svg class="w-6 h-6 text-white" x-show="!showMenu" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor">
              <path d="M4 6h16M4 12h16M4 18h16"></path>
            </svg>
          </button>
        </div>
      </div>
      <div class="mobile-menu hidden md:hidden">
        
        <ul class="mt-4 space-y-4">
            @foreach($arrSidebar as $sidebar)
            <li><a href={{$sidebar['link']}} class="block px-4 py-2 text-white bg-gray-900 rounded">{{$sidebar['text']}}</a></li>
     
            @endforeach
        </ul>
      </div>
      
    </nav>
  </header>