@php
    $today = \Carbon\Carbon::today();
@endphp

<div class="form-group">
    <label for="start_date">Tanggal Mulai:</label>
    <input type="date" id="start_date" name="start_date" class="form-control" value="{{ $today }}">
</div>

<div class="form-group">
    <label for="end_date">Tanggal Akhir:</label>
    <input type="date" id="end_date" name="end_date" class="form-control" value="{{ $today }}">
</div>