@extends('template.Main')

@section('head')
    <!-- Tambahkan CSS dan JavaScript yang diperlukan -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" /> --}}
    {{-- <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet"> --}}
    {{-- <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet"> --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script> --}}
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> --}}

    
    <script src="/vendor/datatables/buttons.server-side.js"></script>
@endsection

@section('content')
@php
    $today = \Carbon\Carbon::now()->format('Y-m-d\TH:i');
@endphp
{{-- @dd($start_date) --}}

<div class="container-fluid w-full text-center  max-w-[79%] p-5 ">
    <div class="row">
        <div class="col-md-6">
        <div class="col-md-6">
            <!-- Filter Tanggal -->
            <form action="{{ route('history.index') }}" method="GET">
                <div class="input-group mb-3">
                    <input id="start_date" type="datetime-local"  name="start_date" class="form-control" placeholder="Tanggal Mulai" id="start_date">
                    <input id="end_date" type="datetime-local"  name="end_date" class="form-control" placeholder="Tanggal Selesai" id="end_date">
                    <button type="submit" class="btn btn-primary">Filter</button>
                </div>
            </form>
        </div>
    </div>

    <!-- Tabel Data -->
    <div class="row ">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">History Data</div>
                <div class="card-body">
                    {{ $dataTable->table() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <!-- Tambahkan script untuk datepicker di sini -->
    <script>
        // $(function () {
        //     $('#start_date, #end_date').datepicker({
        //         format: 'yyyy-mm-dd',
        //         autoclose: true,
        //     });
        // });

        document.addEventListener("DOMContentLoaded", function(event) { 
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            const startDate = urlParams.get('start_date')
            const endtDate = urlParams.get('end_date')
            if(startDate != null){
                start_date.value = startDate
                end_date.value = endtDate
            }else{
                start_date.value = new Date()
                end_date.value = new Date()
            }
    });
    </script>
    <!-- Load JavaScript DataTables -->
    {{ $dataTable->scripts() }}
@endpush
