@extends('template.Main')
@section('head')
<script
src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js">
</script>
{{-- <style>
  #dashboard-main{
    background-image: url('/assets/img/Energy_panel_power_solar_light-512.webp');
  background-repeat: no-repeat;
  background-origin: content-box;
  background-size: 10% auto;
  background-position-x: right;
  background-position-y: bottom;
  } --}}
</style>
@endsection
@section('content')
<div id="dashboard-main" class='bg-white  container-sm my-5 rounded-md h-full p-3  space-y-7'  >

    <div class='w-full flex justify-center text-center'>
    
        @include('component.Card', [
            'title' => 'Kondisi Panel Surya',
            'icon' => '<i class="fa-solid fa-solar-panel text-3xl"></i>',
            'value' => '<span id="panel_value"></span>'
            ])
    </div>
    <div class='w-full text-center d-flex justify-content-center gap-3 '>
        <div class='w-full'>
            @include('component.Card', [
                'title' => 'Tegangan PV',
                'icon' => '<i class="fa-solid fa-bolt text-3xl"></i>',
                'value' => '<span id="tegangan_value"></span>'
                ])
        </div>
        <div class='w-full'>
            @include('component.Card', [
                'title' => 'Arus PV',
                'icon' => '<i class="fa-solid fa-bolt text-3xl"></i>',
                'value' => '<span id="arus_value"></span>'
                ])
        </div>
        <div class='w-full'>
            @include('component.Card', [
                'title' => 'Tegangan Baterai',
                'icon' => '<i class="fa-solid fa-car-battery text-3xl"></i>',
                'value' => '<span id="batt_value"></span>'
                ])
        </div>
        <div class='w-full'>
            @include('component.Card', [
                'title' => 'Intensitas Cahaya',
                'icon' => '<i class="fa-regular fa-sun text-3xl "></i>',
                'value' => '<span id="lux_value"></span>'
                ])
        </div>
    </div>
    


    <div class='w-full flex justify-center'>
      {{-- <canvas id="myChart" style="width:100%;max-width:700px"></canvas> --}}
    </div>
</div>


@stop


@push('script')

<script>
  document.addEventListener("DOMContentLoaded", function(event) { 

    const xValues = [50,60,70,80,90,100,110,120,130,140,150];
  const yValues = [7,8,8,9,9,9,10,11,14,14,15];
  
  new Chart("myChart", {
    type: "line",
    data: {
      labels: xValues,
      datasets: [{
        fill: false,
        lineTension: 0,
        backgroundColor: "rgba(0,0,255,1.0)",
        borderColor: "rgba(0,0,255,0.1)",
        data: yValues
      }]
    },
    options: {
      legend: {display: false},
      scales: {
        yAxes: [{ticks: {min: 6, max:16}}],
      }
    }
  });
  })
  </script>



<script type='module'>
    const interval = 30*1000
    function convertTimeToMinutes(time) {
  let [hours, minutes] = time.split(":").map(Number);
  return hours * 60 + minutes;
}
    function evaluateCondition(voltage, lux, time) {
  let thresholds = [
    { range_time: "08:00-10:00", threshold_voltage: 12.10, threshold_lux: 25000 },
    { range_time: "10:00-12:00", threshold_voltage: 12.10, threshold_lux: 50000 },
    { range_time: "12:00-14:00", threshold_voltage: 12.10, threshold_lux: 50000 },
    { range_time: "14:00-16:00", threshold_voltage: 12.10, threshold_lux: 50000 }
    { range_time: "16:00-18:00", threshold_voltage: 12.10, threshold_lux: 35000 }
  ];

  let threshold = null;
  let currentTime = convertTimeToMinutes(time);
  for (let i = 0; i < thresholds.length; i++) {
    let item = thresholds[i];
    let range = item.range_time.split('-');
    let start = convertTimeToMinutes(range[0]);
    let end = convertTimeToMinutes(range[1]);

    if (currentTime >= start && currentTime <= end) {
      threshold = item;
      break;
    }
  }

  if (!threshold) {
    return "Malam Hari";
  }

  if (voltage > threshold.threshold_voltage && lux < threshold.threshold_lux) {
    return "Normal";
  } else if (voltage < threshold.threshold_voltage && lux < threshold.threshold_lux) {
    return "Normal";
  } else if (voltage > threshold.threshold_voltage && lux > threshold.threshold_lux) {
    return "Normal";
  }

  return "Tidak Normal";
}


    document.addEventListener("DOMContentLoaded", function(event) { 
        const getSensorData = () => {
        return axios.get(`${window.location.href}api/getsensor`)
        .then(function (response) {
        const data = response?.data
        const dataPrev = localStorage.hasOwnProperty("prev") ? JSON.parse(localStorage.getItem("prev")) : null
        const isValPrev = localStorage.hasOwnProperty("prev") ? (JSON.stringify(data) === JSON.stringify(dataPrev)) : false
        localStorage.setItem('prev', JSON.stringify(data))

        const timeNow = new Date().getTime();
        const timeApi = new Date(data?.created_at).getTime();

        const difference = Math.round((timeNow - timeApi) / 1000 / 60);
          console.log({difference}, difference > 1 , isValPrev)
          console.log({data}, {dataPrev})
        const time = new Date(data.created_at)
        const clock = time.getHours() + ":" + time.getMinutes()
        const condition = evaluateCondition(data?.arus_pv, data?.intensitas_cahaya, clock)
                    panel_value.innerText = condition
                    lux_value.innerText = `${data?.intensitas_cahaya} lux`
                    batt_value.innerText = `${data?.tegangan_bat} V`
                    arus_value.innerText = `${data?.arus_pv} A`
                    tegangan_value.innerText = `${data?.tegangan_pv} V`

                    if(difference > 1 && isValPrev){
                      console.log('prev')
                      panel_value.innerText = "Sensor Tidak Terhubung"
                    lux_value.innerText = 0
                    batt_value.innerText = 0
                    arus_value.innerText = 0
                    tegangan_value.innerText = 0
                    }
            
        })}
        getSensorData()
        setInterval(() => {
            getSensorData()
            }, interval);
    });
   
</script>

@endpush