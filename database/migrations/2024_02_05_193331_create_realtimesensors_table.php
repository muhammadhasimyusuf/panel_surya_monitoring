<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('realtimesensors', function (Blueprint $table) {
            $table->id();
            $table->float('tegangan_pv', 8,2);
            $table->float('tegangan_bat', 8,2);
            $table->float('arus_pv', 8,2);
            $table->float('intensitas_cahaya', 8,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('realtimesensors');
    }
};
